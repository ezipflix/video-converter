package edu.esipe.i3.ezipflix.frontend;

import java.net.URI;

/**
 * Created by Gilles GIRAUD gil on 11/4/17.
 */
public class ConversionRequest {

    private String file;
    private String bucket;

    public ConversionRequest() {
    }
    public String getFile() {
        return file;
    }
    public void setFile(String file) {
        this.file = file;
    }
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
    
    
}
