package edu.esipe.i3.ezipflix.frontend.data.repositories;

import edu.esipe.i3.ezipflix.frontend.data.entities.VideoConversions;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;


@Repository
public class VideoConversionRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(VideoConversionRepository.class);

	@Autowired
	private DynamoDBMapper mapper;

	public void save(VideoConversions conversions) {
		mapper.save(conversions);
	}

	public VideoConversions getOneVideoConversions(String videoId, String fileName) {
		return mapper.load(VideoConversions.class, videoId, fileName);
	}


	public DynamoDBSaveExpression buildDynamoDBSaveExpression(VideoConversions videos) {
		DynamoDBSaveExpression saveExpression = new DynamoDBSaveExpression();
		Map<String, ExpectedAttributeValue> expected = new HashMap<String, ExpectedAttributeValue>();
		expected.put("uuid", new ExpectedAttributeValue(new AttributeValue(videos.getUuid()))
				.withComparisonOperator(ComparisonOperator.EQ));
		saveExpression.setExpected(expected);
		return saveExpression;
	}
}
