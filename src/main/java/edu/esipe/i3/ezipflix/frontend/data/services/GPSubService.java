package edu.esipe.i3.ezipflix.frontend.data.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Service;

import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutures;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;

@Service
public class GPSubService {
	
	private static Publisher publisher = null;
	private ProjectTopicName topicName = ProjectTopicName.of("archiDistribueeIng3", "conversion");
	
	public GPSubService() throws IOException {
		publisher = Publisher.newBuilder(this.topicName).build();
	}
	public static void publishMessage(String message) throws Exception {
	    // [START pubsub_publish]
	    
	    
	    try {
	      // Create a publisher instance with default settings bound to the topic
	      
	      ByteString data = ByteString.copyFromUtf8(message);
	      PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();

	      ApiFuture<String> messageIdFuture = publisher.publish(pubsubMessage);
	       
	    } finally {
	      if (publisher != null) {
	        // When finished with the publisher, shutdown to free up resources.
	        publisher.shutdown();
	        publisher.awaitTermination(1, TimeUnit.MINUTES);
	      }
	    }
	    // [END pubsub_publish]
	  }
}
