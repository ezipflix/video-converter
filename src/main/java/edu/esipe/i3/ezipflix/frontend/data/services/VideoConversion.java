package edu.esipe.i3.ezipflix.frontend.data.services;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.fasterxml.jackson.core.JsonProcessingException;
import edu.esipe.i3.ezipflix.frontend.ConversionRequest;
import edu.esipe.i3.ezipflix.frontend.ConversionResponse;
import edu.esipe.i3.ezipflix.frontend.VideoDispatcher;
import edu.esipe.i3.ezipflix.frontend.data.entities.VideoConversions;
import edu.esipe.i3.ezipflix.frontend.data.repositories.VideoConversionRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;

/**
 * Created by Gilles GIRAUD gil on 11/4/17.
 */
@Service
public class VideoConversion {
	private AmazonS3 s3client;

    @Value("${amazonProperties.endpointUrl}")
    private String endpointUrl;
    @Value("${amazonProperties.bucketName}")
    private String bucketName;
    @Value("${amazonProperties.accessKey}")
    private String accessKey;
    @Value("${amazonProperties.secretKey}")
    private String secretKey;
    private static final Logger LOGGER = LoggerFactory.getLogger(VideoConversion.class);

    @Autowired RabbitTemplate rabbitTemplate;

    @Autowired VideoConversionRepository videoConversionRepository;
    GPSubService gPSubService;


    public void save(
                final ConversionRequest request,
                final ConversionResponse response) throws Exception {

        final VideoConversions conversion = new VideoConversions(
                                                    response.getUuid().toString(),
                                                    request.getFile().toString(),
                                                    "",
                                                    "",
                                                    request.getBucket());

        videoConversionRepository.save(conversion);
        final Message message = new Message(conversion.toJson().getBytes(), new MessageProperties());
        //rabbitTemplate.convertAndSend(conversionExchange, conversionQueue,  conversion.toJson());
        this.gPSubService = new GPSubService();
        this.gPSubService.publishMessage(conversion.toJson());
    }
    
    
    @PostConstruct
    private void initializeAmazon() {
       AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
       this.s3client = AmazonS3Client.builder()
    		    .withRegion("eu-west-3")
    		    .withCredentials(new AWSStaticCredentialsProvider(credentials))
    		    .build();
    }
    
    public Boolean checkIfFileExist(String filename, String bucket) {
    	boolean exist = false;
    	ObjectListing ol = this.s3client.listObjects(bucket);
        List<S3ObjectSummary> objects = ol.getObjectSummaries();
        
        LOGGER.info("CHECKIFEXIST");
        for (S3ObjectSummary os: objects) {
            System.out.println("* " + os.getKey());
            if(os.getKey().equals(filename)) { exist = true;}
        }
        return exist;
    }

}
